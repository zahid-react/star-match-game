import React from 'react';
import './App.css';
import StarMatch from './components/star-match'

function App() {
  return (
    <div className="App">
      <StarMatch/>
    </div>
  );
}

export default App;
